# ICD Code Patterns for the Rosie Pattern Language

Matches all ICD-9-CM and ICD-10-CM codes.

## Source
Based on [John D. Cook's work](https://www.johndcook.com/blog/2019/05/05/regex_icd_codes/)

## Full Test Run ICD-10

This will test that all the codes downloaded from the CDC will match (ICD-10, 2019)
`./test-cdc-10.sh `

## TODO
- Write a full test for ICD-9 from the CDC lists 
