#!/usr/bin/env bash

FAILURES=`curl "ftp://ftp.cdc.gov/pub/Health_Statistics/NCHS/Publications/ICD10CM/2019/icd10cm_codes_2019.txt" |  rosie -f idc.rpl grep idc.idc_10 -a 2>&1 > /dev/null`

if [ ! -z "${FAILURES}" ]; then
  echo "Failed to match:"
  echo "${FAILURES}"
  echo "Failure!"
  exit 1
fi
echo "Success, all matched!"
